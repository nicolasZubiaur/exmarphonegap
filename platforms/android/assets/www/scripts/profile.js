/**
 * Created by madam on 2/1/16.
 */
/**
 * Created by madam on 1/22/16.
 */
var pictureSource;   // picture source
var destinationType; // sets the format of returned value 

 function start(){
        $("#saveload").css('display','none');
        $("#state").css('display','none');

        var resultado = JSON.parse(window.localStorage[window.localStorage["user_id"]+"profile"]);

        $('#nameborder').html(resultado.name + ' ' + resultado.second_name);

        $('#profile-headname').html(resultado.name + ' ' + resultado.second_name);

        $('#profile-parentname').html(resultado.father_lastname + ' ' + resultado.mother_lastname);
        //name
        $('#profile-name').val(resultado.name);
        //second_name
        $('#profile-secondname').val(resultado.second_name);
        //father_name
        $('#profile-fathername').val(resultado.father_lastname);
        //mother_name
        $('#profile-mothername').val(resultado.mother_lastname);


        $('#notes').val(resultado.notes);
        //sender

        if(resultado.gender == 'M')
        {
            $('#profile-gender').val('Male');
        }
        else if(resultado.gender == 'F')
        {
            $('#profile-gender').val('Female');
        }
        //email
        $('#profile-email').val(resultado.email);

        //password
        var password = window.localStorage["password"];
        $('#profile-password').val(password);

        //Birthday
        if(!resultado.birthday){
            $('#birthday').val('');
        }else
        {
            var birthday1 =resultado.birthday.split('T')[0];
            var birthday2 = birthday1.split('-');
            $('#birthday').val(birthday2[2] + '/' + birthday2[1] + '/' + birthday2[0]);
        }

        //profiel-passport1
        $('#profile-passport1').val(resultado.passport);
        if(!resultado.passport_expire){
            $('#passport1').val('');
        }else{
            var res_1 =resultado.passport_expire.split('T')[0];
            var res1 = res_1.split('-');
            $('#passport1').val(res1[2]+'/'+res1[1]+'/'+res1[0]);
        }

          //profile-passport2
        $('#profile-passport2').val(resultado.passport2);
        if(!resultado.passport_expire2){
            $('#passport2').val('');
        }else{
            var res_2 = resultado.passport_expire2.split('T')[0];
            var res2 = res_2.split('-');
            $('#passport2').val(res2[2]+'/'+res2[1]+'/'+res2[0]);

        }



        var p=resultado.file;
        if(resultado.file=='')
        {
            $('#profile-photo-url').val('');

        }else
        {
            $('#profile-photo-url').val(p);

        }
        
        if((p!='')&&(p!=null)){
            $('#photouser').css('background-image','url("http://turismoexmar.com/'+p+'")');
            $('#profile-photoimage').css('background-image','url("http://turismoexmar.com/'+p+'")');
            $('#load_image').attr('src','http://turismoexmar.com/'+p);
            $('#file').val(p);
        }
 }

  // Wait for Cordova to connect with the device
    //
    document.addEventListener("deviceready",onDeviceReady,false);

 function onDeviceReady() {
        pictureSource=navigator.camera.PictureSourceType;
        destinationType=navigator.camera.DestinationType;
        
    }

$(document).ready(function() {

    $( "#birthday" ).datepicker({
        dateFormat: 'dd/mm/yy',
        showOn: "button",
        buttonImage: "images/calendar.gif",
        buttonImageOnly: true,
        buttonText: "Select date"

    }).val();
    $( "#passport1" ).datepicker({
        dateFormat: 'dd/mm/yy',
        showOn: "button",
        buttonImage: "images/calendar.gif",
        buttonImageOnly: true,
        buttonText: "Select date"
    });
    $( "#passport2" ).datepicker({
        dateFormat: 'dd/mm/yy',
        showOn: "button",
        buttonImage: "images/calendar.gif",
        buttonImageOnly: true,
        buttonText: "Select date"
    });

    

    var post_token =  window.localStorage["token"];
    var post_userid = window.localStorage["user_id"];

    if(window.localStorage["offline"]=='Off'){
        $.ajax({
            method: "GET",
            url: "http://turismoexmar.com/api/users/" + post_userid,
            dataType: "json",
            context: document.body, beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + post_token);
            }
        }).done(function(result) {
            window.localStorage[window.localStorage["user_id"]+"profile"]=JSON.stringify(result.data);
            start();

        }).fail(function(request){
            $("#saveload").css('display','none');
            $("#state").css('display','none');
            var jsonr = JSON.parse(request.responseText);
            console.log(post_token);
            console.log(jsonr);
            if(jsonr.success==false){
                if(jsonr.data.message='Expired token'){
                    navigator.notification.alert('Favor de volver a ingresar tus credenciales');
                    window.localStorage["token"]='';
                    location.href="index.html";
                }else{
                    navigator.notification.alert('Ha ocurrido un error, favor de tratar más tarde',null,'error','OK');
                }
            }else{
                navigator.notification.alert('Ha ocurrido un error, favor de tratar más tarde',null,'error','OK');
            }
        });
    }else{
        $("#saveload").css('display','none');
        $("#state").css('display','none');
            alert(window.localStorage["token"]);
        if(window.localStorage[window.localStorage["user_id"]+"profile"]==''){
            $('#alertoffline').show(); 
        }else if(!window.localStorage[window.localStorage["user_id"]+"profile"]){
           $('#alertoffline').show(); 
        }else{
            $("#saveload").css('display','block');
            $("#state").css('display','block');
            start();
        }
    }
    

    $("#save_button").click(function () {
        $("#saveload").css('display','block');
        $("#state").css('display','block');

        var profile_name = $('#profile-name').val();

        var profile_secondname = $('#profile-secondname').val();

        var profile_fathername = $('#profile-fathername').val();

        var profile_mothername = $('#profile-mothername').val();


        var profile_sendgender = $('#profile-gender').val();

        var profile_gender;
        if(profile_sendgender=='Male') {
            profile_gender = 'M';
        }
        else
        {
                profile_gender = 'F';
        }

        var profile_email = $('#profile-email').val();

        var profile_password = $('#profile-password').val();

        var profile_birthday1 = $('#birthday').val().split('/');
        var profile_birthday = profile_birthday1[2] + '-' + profile_birthday1[1] + '-' + profile_birthday1[0];

        var profile_passport1 = $('#profile-passport1').val();


        var profile_passportdate1 = $('#passport1').val().split('/');
        var passportdate1;
        if(profile_passportdate1=='')
        {
            passportdate1="";
        }
        else{
            passportdate1 = profile_passportdate1[2] + '-' + profile_passportdate1[1] + '-' + profile_passportdate1[0];
        }

        var profile_passport2 = $('#profile-passport2').val();

        var profile_passportdate2 = $('#passport2').val().split('/');
        var passportdate2;
        if(profile_passportdate2=='')
        {
            passportdate2="";
        }
        else{
         passportdate2 = profile_passportdate2[2] + '-' + profile_passportdate2[1] + '-' + profile_passportdate2[0];
        }

        if($('#profileimg').val()=='load'){
            var largeImage = document.getElementById('load_image');
            var canvas = document.createElement("canvas");
            var ctx = canvas.getContext("2d");
            var maxW = 800;
            var maxH = 800;
            var scale = Math.min((maxW / largeImage.naturalWidth), (maxH / largeImage.naturalHeight));
            if (scale < 1) {
                //largeImage.onload= function(){
                    canvas.width = largeImage.naturalWidth * scale;
                    canvas.height = largeImage.naturalHeight * scale;
                    ctx.drawImage(largeImage, 0, 0, largeImage.naturalWidth, largeImage.naturalHeight, 0, 0, canvas.width, canvas.height);
               //};
            }else {
                //largeImage.onload= function(){
                    canvas.width = largeImage.naturalWidth;
                    canvas.height = largeImage.naturalHeight;
                    ctx.drawImage(largeImage, 0, 0, canvas.width, canvas.height);
               // };
            }
            var dataURL = canvas.toDataURL("image/png");
            $('#profileimg').val(dataURL);
            
            //assign value to var
            var profile_photo_url = $('#profileimg').val();
        }else{
            var profile_photo_url = $('#file').val();
        }

        var notes = $('#notes').val();

        $.ajax({
            method: "PUT",
            url: "http://turismoexmar.com/api/users/" + post_userid,
            dataType: "json",
            context: document.body, beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + post_token)
            },
            data: {
                id: post_userid,
                name: profile_name,
                second_name: profile_secondname,
                father_lastname: profile_fathername,
                mother_lastname: profile_mothername,
                gender: profile_gender,
                email: profile_email,
                password: profile_password,
                birthday: profile_birthday,
                passport: profile_passport1,
                passport_expire: passportdate1,
                passport2: profile_passport2,
                passport_expire2: passportdate2,
                file: profile_photo_url,
                notes:notes
            }
        }).done(function(result2) {

            navigator.notification.alert("Tus datos han sido guardados! Si no ves los cambios podrías estar en Offline mode.",null,'Aviso','OK');
 
            $("#saveload").css('display','none');
            $("#state").css('display','none');
            location.reload();

        }).fail(function(result2){
            $("#saveload").css('display','none');
            $("#state").css('display','none');
            navigator.notification.alert("Ha ocurrido un error, intenta nuevamente",null,'Error','OK');
        });
    });


});

// Called when a photo is successfully retrieved
    //
    function onPhotoDataSuccess(imageData) {
        var largeImage = document.getElementById('load_image');
        largeImage.src = imageData;
        $('#profileimg').val('load');
        $("#saveload").css('display','none');
        $("#state").css('display','none');
    }

    // Called when a photo is successfully retrieved
    //
    function onPhotoURISuccess(imageURI) {
        var largeImage = document.getElementById('load_image');
        largeImage.src = imageURI;
        $('#profileimg').val('load');
        $("#saveload").css('display','none');
        $("#state").css('display','none');
    }

    // A button will call this function
    //
    function capturePhotoEdit() {
        $("#saveload").css('display','block');
        $("#state").css('display','block');
          // Take picture using device camera, allow edit, and retrieve image as base64-encoded string  
          navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 50, correctOrientation: true }); 
    }

    // A button will call this function
    //
    function getPhoto(source) {
        $("#saveload").css('display','block');
        $("#state").css('display','block');
      // Retrieve image file location from specified source
      navigator.camera.getPicture(onPhotoURISuccess, onFail, { quality: 50, 
        destinationType: destinationType.FILE_URI,
        correctOrientation: true,
        sourceType: source });
    }

    // Called if something bad happens.
    // 
    function onFail(message) {
        navigator.notification.alert('Hubo un error: ' + message);
        $("#saveload").css('display','none');
        $("#state").css('display','none');
    }


