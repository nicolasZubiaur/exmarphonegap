/**
 * Created by madam on 2/9/16.
 */

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
function getDate(start_date){

    var first_date=start_date;
    var second_date = first_date.split("-");
    var stringToParse=second_date[0]+"/"+second_date[1]+"/"+second_date[2];
    var dateString    = stringToParse.match(/\d{4}\/\d{2}\/\d{2}\s+\d{2}:\d{2}/);
    var dt  = new Date(dateString);
    var dr = dt.toString();
    var q = dr.split("GMT")[0];
    var w=q.split(":");
    var e=w[0]+":"+w[2];
    var r= e.split(" ");
    var list_date= "  "+r[0]+" "+r[2]+" "+r[1]+" "+r[3]+" "+r[4];
    return list_date;
}

function start(){
    $("#saveload").css('display','none');
        $("#state").css('display','none');

        var start_f;
        var departcity;
        var reservation;
        var comments;
        var userslist="";
        var groups="";
        var weather="";
        var weather2="";
        var currency="";
        var dev_type;

        var resultado = JSON.parse(window.localStorage["result_details1"+getParameterByName("destination_id")]);
        
        for (var i=0;i<resultado.destination.length;i++)
        {
            start_f=resultado.destination[i].start_date;
            departcity=resultado.destination[i].departure_city_name+" <i class='fa fa-arrow-right'></i> "+resultado.destination[i].arrival_city_name;
            reservation=resultado.destination[i].reservation;
            comments=resultado.destination[i].comments;
            var ar = ["fa-plane", "fa-train", "fa-car", "fa-ship", "fa-bus", "fa-rocket"];
            var n = resultado.destination[i].type;
            dev_type = ar[n];
            console.log(dev_type)
        }

        for (var i=0;i<resultado.users.length;i++)
        {

           var  users=resultado.users[i].user;
            userslist+='<span class="padleft">'+users+
            '</span> | ';
        }
        for (var i=0;i<resultado.groups.length;i++)
        {

            var grouplist=resultado.groups[i].user;
            groups+='<span class="padleft">'+grouplist+
            '</span> | ';
        }

        for (var i=0;i<resultado.weather.length;i++)
        {
            c = (resultado.weather[i].temp_arrival-32)*0.5556;
            weather2 +='<span>Clima actual en: '+resultado.destination[i].arrival_city_name+'</span>';
            weather2 += '<div class="right-image1"><img src="http://l.yimg.com/a/i/us/we/52/'+resultado.weather[i].code_arrival+'.gif"/></div>'
            weather2 += '<span>'+Math.round(c)+'° C, '+resultado.weather[i].text_arrival+'</span>';

            
        }
       
        if(resultado.currency!=''){
            currency += ' 1 '+resultado.currency.from+' <i class="fa fa-arrow-right"></i> '+Math.round(resultado.currency.to.col1)+' MXN';
        }
        

        var start=getDate(start_f);
        

        $('#start_date').html(start);
        $('#depart_name').html(departcity);
        $('#reservation').html(reservation);
        $('#comments').html(comments);
        $('#users_list').append(userslist);
        $('#group_list').append(groups);
        $('#weather').append(weather);
        $('#weather2').append(weather2);
        $('#currency').append(currency);
}

$(document).ready(function() {

    var post_token =  window.localStorage["token"];
    var destination_id=getParameterByName("destination_id");

    if(window.localStorage["offline"]=='Off'){
        $("#preloader").css('display','block');
        $("#status").css('display','block');

        $.ajax({
            method: "GET",
            url: "http://turismoexmar.com/api/itineraries_cities/details/" + destination_id,
            dataType: "json",
            context: document.body, beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + post_token);
            }
        }).done(function (result) {
            window.localStorage["result_details1"+destination_id] = JSON.stringify(result.data);
            start();

        }).fail(function(result){
               $("#saveload").css('display','none');
               $("#state").css('display','none');
               var jsonr = JSON.parse(result.responseText);
                if(jsonr.success==false){
                    if(jsonr.data.message='Expired token'){
                        navigator.notification.alert('Favor de volver a ingresar tus credenciales');
                        window.localStorage["token"]='';
                        location.href="index.html";
                    }else{
                        navigator.notification.alert('Ha ocurrido un error, favor de tratar más tarde',null,'error','OK');
                    }
                }else{
                    navigator.notification.alert('Ha ocurrido un error, favor de tratar más tarde',null,'error','OK');
                }
        });
    }else{
        $("#saveload").css('display','none');
        $("#state").css('display','none');
        if(window.localStorage["result_details1"+destination_id]==''){
            $('#alertoffline').show(); 
        }else if(!window.localStorage["result_details1"+destination_id]){
            $('#alertoffline').show(); 
        }else{
            $("#saveload").css('display','block');
            $("#state").css('display','block');
            var resultado = JSON.parse(window.localStorage["result_details1"+destination_id]);
            for (var i=0;i<resultado.destination.length;i++)
            {
                if(destination_id==resultado.destination[i].id){
                    start();
                }else{
                    $('#alertoffline').show();  
                }
            }
        }
    }

    $('#back').click(function(){
        
        if(device.platform === "iOS" && parseInt(device.version) === 9){
            var resultado = JSON.parse(window.localStorage["result_details1"+destination_id]);
            for (var i=0;i<resultado.destination.length;i++)
            {
                itinerary = resultado.destination[i].itinerary_id;
            }
            location.href="travel_list.html?itinerary_id="+itinerary;
        }else{
            navigator.app.backHistory();
        }
    });

});
